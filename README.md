**example_1**  
Creating the game 'Bulls and cows'. The program generates a number and you  
must guess it. The correct digit of a correct position is 'bull' and the  
correct digit of a wrong position is 'cow'.  

**example_2**  
*The first part*  
Creating a program which receives credit card number and checks if the card  
can exist.  
*The second part*  
Creating a function which takes the list of names and return a string describing  
the number of likes.  
 
**example_3**  
*The first part*  
Creating a calculator which can conduct such operations as addition,  
subtraction, multiplication and division.  
*The second part*  
Creating a program which realizes Caesar cipher.  

**example_4**  
*The first part*  
Creating the decorator of types. It checks the parameters of function,  
converts them and sums if it necessary.  
*The second part*  
Creating a program which finds symbol '#' and if it is, deletes the previous  
symbol.  

**example_5**  
The realisation of a library. Program consists of three classes (Books, User,  
Library). Each user can take, return and reserve any book.  

**example_6**  
Realisation of a bank deposit.  

**example_7**  
Creating an engineering calculator. The calculator can include any quantity  
of brackets.  

**example_8**  
Creating tests using framework Unittest.  

**example_9**  
Creating tests using framework Pytest.  

**example_10**  
Finding elements, entering the buttons and entering the data into the  
fields in the website  
<https://ultimateqa.com/simple-html-elements-for-automation/>  
using Selenium Webdriver.  

**example_11**  
*Task1*  
Creating different requests using the service  
<https://fakerestapi.azurewebsites.net>  
*Task2*  
Working with data from XML file.  
*Task3*  
Working with data from JSON file.  
*Task4*  
Working with data from YAML file.  

**example_12**  
Working with databases using MySQL. Creating requests.   
