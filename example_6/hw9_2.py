class Deposit:
    def __init__(self, summ, time):
        self.summ = summ
        self.time = time
        self.percent = 0.1

    def bank(self):
        for i in range(self.time):
            self.summ += self.summ * self.percent
        return self.summ


deposit = Deposit(1000, 4)
print(deposit.bank())
