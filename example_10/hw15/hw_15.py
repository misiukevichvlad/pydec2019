from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

# 1
browser = webdriver.Chrome("")
browser.get("https://ultimateqa.com/simple-html-elements-for-automation/")
try:
    el1_1 = browser.find_element(By.CSS_SELECTOR,
                                 ".et_pb_text_inner "
                                 "#htmlTableId:nth-child(2) th:nth-child(2)")
    el2_1 = browser.find_element(By.CSS_SELECTOR,
                                 ".et_pb_module.et_pb_text.et_pb_text_4 "
                                 "th:nth-child(2)")
    assert el1_1 != el2_1
    assert el1_1.text == el2_1.text
    el1_2 = browser.find_element(By.CSS_SELECTOR,
                                 ".et_pb_text_inner #htmlTableId:nth-child(2)"
                                 " tr:nth-child(2) td:nth-child(1)")
    el2_2 = browser.find_element(By.CSS_SELECTOR,
                                 ".et_pb_module.et_pb_text.et_pb_text_4 "
                                 "tr:nth-child(2) td:nth-child(1)")
    assert el1_2 != el2_2
    assert el1_2.text == el2_2.text
    el1_3 = browser.find_element(By.CSS_SELECTOR,
                                 ".et_pb_text_inner #htmlTableId:nth-child(2)"
                                 " tr:nth-child(3) td:nth-child(3)")
    el2_3 = browser.find_element(By.CSS_SELECTOR,
                                 ".et_pb_module.et_pb_text.et_pb_text_4 "
                                 "tr:nth-child(3) td:nth-child(3)")
    assert el1_3 != el2_3
    assert el1_3.text == el2_3.text
    el1_4 = browser.find_element(By.CSS_SELECTOR,
                                 ".et_pb_text_inner #htmlTableId:nth-child(2)"
                                 " tr:nth-child(4) td:nth-child(2)")
    el2_4 = browser.find_element(By.CSS_SELECTOR,
                                 ".et_pb_module.et_pb_text.et_pb_text_4 "
                                 "tr:nth-child(4) td:nth-child(2)")
    assert el1_4 != el2_4
    assert el1_4.text == el2_4.text

# 2
    a = browser.find_element(By.NAME, "et_pb_contact_name_0")
    a.send_keys('Vlad')
    b = browser.find_element(By.NAME, "et_pb_contact_email_0")
    b.send_keys('m.vlad1993@yandex.ru')
    button_1 = browser.find_element(By.NAME, "et_builder_submit_button")
    button_1.click()

# 3
    button_tab1 = browser.find_element(By.CSS_SELECTOR,
                                       ".et_pb_tabs_controls .et_pb_tab_0")

    button_tab2 = browser.find_element(By.CSS_SELECTOR,
                                       ".et_pb_tabs_controls .et_pb_tab_1")
    button_3 = browser.find_element(By.CSS_SELECTOR,
                                    ".et_pb_section.et_pb_section_3 "
                                    ".et_pb_module.et_pb_blurb."
                                    "et_pb_blurb_11 #button1")
    ActionChains(browser).move_to_element(button_3).perform()
    button_tab1.click()
    button_tab2.click()
finally:
    browser.quit()
