# 1.Validate
def func():
    while True:
        card = input("Введите 16-значное число: ")
        if len(card) != 16 or not card.isdigit():
            print('Ошибка ввода! Попробуйте ещё раз.')
            continue
        else:
            card = str(card)
            card_sum = 0
            for i in range(len(card)):
                if i % 2 == 0:
                    if int(card[i]) * 2 > 9:
                        card_sum += int(card[i]) * 2 - 9
                    else:
                        card_sum += int(card[i]) * 2
                else:
                    card_sum += int(card[i])
            if card_sum % 10 == 0:
                print('True')
            else:
                print('False')
            break


func()


# 2. Like
def likes(*args):
    if len(args) == 0:
        print('no one likes this')
    elif len(args) == 1:
        print(f'{args[0]} likes this')
    elif len(args) == 2:
        print(f'{args[0]} and {args[1]} like this')
    elif len(args) == 3:
        print(f'{args[0]}, {args[1]} and {args[2]} like this')
    elif len(args) > 3:
        print(f'{args[0]}, {args[1]} and {(len(args) - 2)} others like this')


likes()
likes("Ann")
likes("Ann", "Alex")
likes("Ann", "Alex", "Mark")
likes("Ann", "Alex", "Mark", "Max")
likes("Ann", "Alex", "Mark", "Max", "Victor", "Ali")
