import sys
import numexpr


def data():
    b = ['-', '/', '*', '+', '(', ')', ' ']
    numb = input('~ ')
    for i in numb:
        if i.isdigit() or i in b:
            continue
        print('Введены некорректные данные!')
        sys.exit()
    a = []
    for i in numb:
        if i != ' ':
            a.append(i)
        continue
    check_and_result(a)


def check_and_result(list1):
    for i in range(len(list1)):
        char = list1[i]
        next = list1[i + 1] if i + 1 < len(list1) else None
        if list1[0] in '/*+)':
            print('Выражение не может начинаться с'
                  ' символов "+", "/", "*", ")"')
            sys.exit()
        elif list1[-1] in '/*+(-':
            print('Выражение не может заканчиваться'
                  ' символами "+", "/", "*", "(", "-"')
            sys.exit()
        elif char == '(':
            if not next.isdigit() and next not in '-(':
                print('Ошибка после символа "("')
                sys.exit()
        elif char == ')' and next:
            if next not in ')*/-+':
                print('Ошибка после символа ")"')
                sys.exit()
        elif char.isdigit() and next:
            if not next.isdigit() and next not in '+-*/)':
                print('Ошибка после ввода цифры')
                sys.exit()
        elif char == '-':
            if not next.isdigit() and next != '(':
                print('Ошибка после символа "-"')
                sys.exit()
        elif char == '+':
            if not next.isdigit() and next not in '(-':
                print('Ошибка после символа "+"')
                sys.exit()
        elif char == '/':
            if next == '0':
                print('На ноль делить нельзя!')
                sys.exit()
            elif not next.isdigit() and next not in '(-':
                print('Ошибка после символа "/"')
                sys.exit()
        elif char == '*':
            if not next.isdigit() and next not in '(-*':
                print('Ошибка после символа "*"')
                sys.exit()
    print(numexpr.evaluate(''.join(list1)))


data()
