import mysql.connector as mysql

db = mysql.connect(
    host="localhost",
    user="root",
    passwd="5466",
    database="hw_18"
)

cursor = db.cursor()

cursor.execute("CREATE TABLE orders (ord_no  VARCHAR(10), purch_amt VARCHAR(10),"
               " ord_date VARCHAR(10), customer_id VARCHAR(10), salesman_id VARCHAR(10))")

query = "INSERT INTO orders (ord_no, purch_amt, ord_date, customer_id," \
        " salesman_id) VALUES (%s, %s, %s, %s, %s)"
values = [
    (70001, 150.5, 10.05, 3005, 5002),
    (70009, 270.65, 09.10, 3001, 5005),
    (70002, 65.26, 10.05, 3002, 5001),
    (70004, 110.5, 08.17, 3009, 5003),
    (70007, 948.5, 09.10, 3007, 5002),
    (70005, 2400.6, 07.27, 3002, 5001),
    (70008, 5760, 09.10, 3004, 5001),
    (70010, 1983.43, 10.10, 3009, 5006),
    (70003, 2480.4, 10.10, 3008, 5003),
    (70012, 250.45, 06.27, 3003, 5002)
]
cursor.executemany(query, values)
db.commit()
print(cursor.rowcount, "records inserted")
query = "SELECT ord_no, purch_amt, ord_date FROM orders WHERE salesman_id = 5002"
cursor.execute(query)
print(cursor.fetchall())
query = "SELECT distinct salesman_id FROM orders"
cursor.execute(query)
print(cursor.fetchall())
query = "SELECT ord_date, salesman_id, ord_no, purch_amt FROM orders"
cursor.execute(query)
print(cursor.fetchall())
query = "SELECT * FROM orders WHERE ord_no between 70001 and 70007"
cursor.execute(query)
print(cursor.fetchall())
db.close()
