# 1 Декоратор типов
def decorator(func):
    def wrapper(*args):
        str_list = []
        int_list = []
        for i in args:
            if type(i) == int:
                int_list.append(i)
            elif type(i) == str and i.isdigit():
                int_list.append(int(i))
            elif type(i) == str and not i.isdigit():
                str_list.append(i)
            elif type(i) == float:
                int_list.append(i)
        func(str_list, int_list)

    return wrapper


@decorator
def add(str_list, int_list):
    if str_list:
        print(''.join(str_list))
    else:
        print(sum(int_list))


add(47, 85, 4, 98, 12)
add(54, '32')
add('one', 'two', 'three')
add(0.4, 7.12, 0.1, 0.01)


# 2 Строки с заданным символом
def string():
    new_string = input('Введите строку: ')
    new_list = []
    for i in new_string:
        new_list.append(i)
    return ''.join(delete_element(new_list))


def delete_element(correct_list):
    for i in correct_list:
        if i == '#':
            a = correct_list.index(i)
            del correct_list[a]
            if a > 0:
                del correct_list[a - 1]
            delete_element(correct_list)
    return correct_list


print(string())
