# Простейший калькулятор
def numbers():
    a = ['0', '5', '6', '7', '8', '9']
    while True:
        number = input('Выберите операцию: \n 1 - сложение \n'
                       ' 2 - вычитание \n 3 - умножение \n 4 - деление \n'
                       'Введите номер пункта меню: ')
        if len(number) != 1 or not number.isdigit() or number in a:
            print("Ошибка ввода, попробуйте ещё раз!")
            continue
        else:
            break
    while True:
        number_1 = input("Введите первое число: ")
        if not number_1.isdigit():
            print("Введено не число!")
            continue
        else:
            break
    while True:
        number_2 = input("Введите второе число: ")
        if not number_2.isdigit():
            print("Введено не число!")
            continue
        else:
            break
    calculate(number, number_1, number_2)


def calculate(z, x, y):
    if int(z) == 1:
        print(f'Сложение: {int(x) + int(y)}')
    elif int(z) == 2:
        print(f'Вычитание: {int(x) - int(y)}')
    elif int(z) == 3:
        print(f'Умножение: {int(x) * int(y)}')
    elif int(z) == 4 and int(y) == 0:
        print("На ноль делить нельзя!")
        numbers()
    elif int(z) == 4 and int(y) != 0:
        print(f'Частное: {int(x) // int(y)}, Остаток: {int(x) % int(y)}')


numbers()


# Шифр цезаря

def values():
    while True:
        text = input('Введите строку: ')
        if text != '':
            break
        else:
            print('Вы ввели пустую строку! Попробуйте ещё раз')
            continue
    while True:
        digit = input('Введите целое число: ')
        if digit.isdigit():
            break
        else:
            print('Ошибка! Попробуйте ещё раз!')
            continue
    while True:
        operation_number = input('1 - Зашифровка\n'
                                 '2 - Расшифровка\n'
                                 'Введите номер операции: ')
        if operation_number in ['1', '2']:
            break
        else:
            print('Ошибка! Попробуйте ещё раз!')
            continue
    if operation_number == '1':
        decode(text, digit)
    else:
        encode(text, digit)


def decode(string, number):
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    new_list = []
    for letter in string:
        if letter in alphabet:
            new_number = alphabet.index(letter)
            new_list.append(alphabet[(int(number) + new_number) % 26])
        else:
            new_list.append(letter)
    print(''.join(new_list))


def encode(string, number):
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    new_list = []
    for letter in string:
        if letter in alphabet:
            new_number = alphabet.index(letter)
            new_list.append(alphabet[(new_number - int(number)) % 26])
        else:
            new_list.append(letter)
    print(''.join(new_list))


values()
