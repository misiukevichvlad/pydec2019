class Book(object):
    def __init__(self, title, author, pages_count, isbn):
        self.title = title
        self.author = author
        self.pages_count = pages_count
        self.isbn = isbn


class User(object):
    def __init__(self, full_name, library):
        self.fullname = full_name
        self.library = library

    def reserve_book(self, book):
        self.library.reserve_book(self.fullname, book)

    def get_book(self, book):
        self.library.take_book(self.fullname, book)

    def return_book(self, book):
        self.library.return_book(self.fullname, book)


class Library(object):
    def __init__(self):
        self.existing_books = []
        self.reserved_by = self.taken_by = {}

    def append_new_book(self, book):
        self.existing_books.append(book)

    def get_book_status(self, book):
        if book not in self.existing_books:
            return 4, "BOOK DOESN'T EXIST"
        if book.isbn in self.reserved_by.keys():
            return 2, "BOOK IS RESERVED"
        elif book.isbn in self.taken_by.keys():
            return 3, "BOOK IS TAKEN"
        else:
            return 1, "BOOK IS AVAILABLE"

    def reserve_book(self, user_name, book):
        book_status_number, book_status_message = self.get_book_status(book)
        if book_status_number == 1:
            print("Book '{}' by author '{}' has been reserved "
                  "Successfully".format(book.title, book.author))
            self.reserved_by[book.isbn] = user_name
        else:
            print("The Book cannot be reserved by the reason: "
                  "{}".format(book_status_message))

    def take_book(self, user_name, book):
        book_status_number, book_status_message = self.get_book_status(book)
        if book_status_number == 2 and self.reserved_by[book.isbn] == user_name:
            print("You have take the Book '{}' by author '{}'"
                  " successfully".format(book.title, book.author))
            self.reserved_by[book.isbn] = None
            self.taken_by[book.isbn] = user_name
        elif book_status_number == 1:
            print("You should RESERVE the Book firstly")
        else:
            print("The Book cannot be reserved by the reason:"
                  " {}".format(book_status_message))

    def return_book(self, user_name, book):
        book_status_number, book_status_message = self.get_book_status(book)
        if book_status_number == 3 and self.reserved_by[book.isbn] == user_name:
            print("Thanks for returning Book {}".format(book.title))
            self.taken_by[book.isbn] = None
