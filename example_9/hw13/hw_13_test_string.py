import pytest
from hw8_2 import string


@pytest.mark.equals
def test_equal1():
    assert string('123####add#') == 'ad'


@pytest.mark.equals
def test_equal2():
    assert string('#lk#') == 'l'


@pytest.mark.equals
def test_true():
    assert string('na#######me##me'), True


@pytest.mark.equals
def test_false():
    assert string('') != 'aa'


def test_equal3():
    assert string('hello#') == 'hell'


@pytest.mark.parametrize('test_input', ['#33gj####', 'xxx##3##', '#####'])
def test_param(test_input):
    assert string(test_input) == '', 'error'


@pytest.fixture
def func():
    return 'Негативный тест'


def test_negative(func):
    print(func)
    assert string('q##') != 'q'


def test_negative1(func):
    print(func)
    assert string('na#######me##me') != 'name'
