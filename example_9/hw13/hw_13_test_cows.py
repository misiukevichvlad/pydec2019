import pytest
from hw5 import count


def test_true1():
    assert count(''), True


def test_equal1():
    assert count('3214') == 'Коров - 2, быков - 2'


def test_equal2():
    assert count('8735') == 'Коров - 0, быков - 1'


@pytest.mark.equals
def test_equal3():
    assert count('7785') == 'Ошибка ввода!'


@pytest.mark.equals
def test_equal4():
    assert count('1234') == 'Вы выиграли!'


@pytest.mark.equals
def test_true():
    assert count('1874'), True


@pytest.mark.parametrize('test_input', ['1224', '2200', '6557'])
def test_param(test_input):
    assert count(test_input) == 'Ошибка ввода!', 'error'


@pytest.fixture
def func():
    return 'Негативный тест'


def test_negative(func):
    print(func)
    assert count('217') != 'Вы выиграли!'


def test_negative1(func):
    print(func)
    assert count('hol') != 'Коров - 1, быков - 2'
