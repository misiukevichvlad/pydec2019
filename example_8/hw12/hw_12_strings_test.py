import unittest
import hw8_2


class Test(unittest.TestCase):
    def test_equal(self):
        self.assertEqual(hw8_2.string('hc###rgjg'), 'rgjg')

    def test_not_equal(self):
        self.assertNotEqual(hw8_2.string('1##llmk##ho'), 'llh')

    def test_true(self):
        self.assertTrue(hw8_2.string('abcd###'))

    def test_false(self):
        self.assertFalse(hw8_2.string('ab##cd#####'))

    def test_not_none(self):
        self.assertIsNotNone(hw8_2.string('123#############'))

    def test_equal1(self):
        self.assertEqual(hw8_2.string('###'), '')

    def test_equal2(self):
        self.assertEqual(hw8_2.string('####plk'), 'plk')

    @unittest.expectedFailure
    def test_negative1(self):
        self.assertEqual(hw8_2.string('###asd##'), '')

    @unittest.expectedFailure
    def test_negative2(self):
        self.assertEqual(hw8_2.string('su#pp#'), 'spp')


if __name__ == '__main__':
    unittest.main()
