def func():
    n = input("Введите 4-значное число с неповторяющимися цифрами: ")
    print(count(n))


def count(a):
    list1 = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
    if (len(a) != 4 or a[0] not in list1 or a[1] not in
            list1 or a[2] not in list1 or a[3] not in list1):
        return "Ошибка ввода!"
    if a[0] == a[1] or a[0] == a[2] or a[0] == a[3] \
            or a[1] == a[2] or a[1] == a[3] or a[2] == a[3]:
        return "Ошибка ввода!"
    q = '1234'
    if q[0] == a[0] and q[1] == a[1] and q[2] == a[2] \
            and q[3] == a[3]:
        return "Вы выиграли!"
    else:
        count_cor = 0
        count_byk = 0
        if q[0] == a[1] or q[0] == a[2] or q[0] == a[3]:
            count_cor += 1
        if q[1] == a[0] or q[1] == a[2] or q[1] == a[3]:
            count_cor += 1
        if q[2] == a[0] or q[2] == a[1] or q[2] == a[3]:
            count_cor += 1
        if q[3] == a[0] or q[3] == a[1] or q[3] == a[2]:
            count_cor += 1
        if q[0] == a[0]:
            count_byk += 1
        if q[1] == a[1]:
            count_byk += 1
        if q[2] == a[2]:
            count_byk += 1
        if q[3] == a[3]:
            count_byk += 1
        return f"Коров - {count_cor}, быков - {count_byk}"

# func()
