import unittest
import hw5


class Test(unittest.TestCase):
    def test_equal(self):
        self.assertEqual(hw5.count('1234'), 'Вы выиграли!')

    def test_equal1(self):
        self.assertEqual(hw5.count('0234'), 'Коров - 0, быков - 3')

    def test_equal2(self):
        self.assertEqual(hw5.count('1'), 'Ошибка ввода!')

    def test_equal3(self):
        self.assertEqual(hw5.count(''), 'Ошибка ввода!')

    def test_not_equal(self):
        self.assertNotEqual(hw5.count('3210'), 'Коров - 2, быков - 2')

    def test_true(self):
        self.assertTrue(hw5.count('5641'))

    def test_not_none(self):
        self.assertIsNotNone(hw5.count('5555'))

    def test_equal4(self):
        self.assertEqual(hw5.count('hello'), 'Ошибка ввода!')

    @unittest.expectedFailure
    def test_negative1(self):
        self.assertEqual(hw5.count('5422'), 'Коров - 1, быков - 1')

    @unittest.expectedFailure
    def test_negative2(self):
        self.assertEqual(hw5.count('1234'), 'Ошибка ввода!')

    @unittest.expectedFailure
    def test_negative3(self):
        self.assertEqual(hw5.count('1234'), 'Коров - 0, быков - 4')


if __name__ == '__main__':
    unittest.main()
