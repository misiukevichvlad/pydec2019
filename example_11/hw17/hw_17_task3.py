import json

with open('students.json', 'r') as f:
    data = json.load(f)


def find_pupils_by_name(students):
    name = 'Koharu'
    for pupil in students:
        if name in pupil['Name']:
            print(pupil['Name'])


def organize_by_gender(students):
    boys = []
    girls = []
    for pupil in students:
        if pupil['Gender'] == "M":
            boys.append(pupil['Name'])
        else:
            girls.append(pupil['Name'])
    print(f'boys: {boys}')
    print(f'girls: {girls}')


def find_pupil_by_class(students):
    list1 = []
    for pupil in students:
        if pupil['Class'] == '5a':
            list1.append(pupil['Name'])
    print(f'pupils in class "5a": {list1}')


def find_pupils_by_club(students):
    list2 = []
    for pupil in students:
        if pupil['Club'] == 'Chess':
            list2.append(pupil['Name'])
    print(f'pupils in club "chess": {list2}')


find_pupils_by_name(data)
organize_by_gender(data)
find_pupil_by_class(data)
find_pupils_by_club(data)
