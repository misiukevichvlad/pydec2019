import xml.etree.cElementTree as ET

tree = ET.ElementTree(file='library.xml')
root = tree.getroot()


def find_books_by_author(ex1, number):
    authors = []
    for child in ex1:
        for i in child:
            if i.tag == 'author':
                authors.append(i.text)
    return authors[number]


def find_books_by_title(ex2, number):
    titles = []
    for child in ex2:
        for i in child:
            if i.tag == 'title':
                titles.append(i.text)
    return titles[number]


def find_books_by_price(ex3, number):
    prices = []
    for child in ex3:
        for i in child:
            if i.tag == 'price':
                prices.append(i.text)
    return prices[number]


def find_books_by_description(ex4, number):
    descriptions = []
    for child in ex4:
        for i in child:
            if i.tag == 'description':
                descriptions.append(i.text)
    return descriptions[number]


def choose_a_book(root):
    a = find_books_by_author(root, 2)
    b = find_books_by_title(root, 0)
    c = find_books_by_price(root, 3)
    for i in (a, b, c):
        if i in ('Gambardella, Matthew', "XML Developer's Guide", '44.95',
                 'An in -depth look at creating applications with XML.'):
            print('id="bk101"')
        if i in ('Corets, Eva', 'Maeve Ascendant', '7',
                 'After the collapse of a nanotechnology society in England,'
                 ' the young survivors lay the foundation for a new society.'):
            print('id="bk103"')
        if i in ('Robert C. Martin', 'Clean Code', '36.75', 'Even bad code can function. '
                 'But if code isn’t clean, it can bring a development'
                 ' organization to its knees.'):
            print('id="bk104"')


choose_a_book(root)
