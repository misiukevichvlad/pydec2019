import requests

# authors list
response = requests.get("https://fakerestapi.azurewebsites.net/api/Authors").json()
print(response)
authors = []
for i in response:
    if i['FirstName']:
        authors.append(i['FirstName'])
print(authors)

# get author by id
for i in response:
    if i['ID'] == 2:
        print(i['FirstName'])

# post book
response_1 = requests.post('https://fakerestapi.azurewebsites.net/api/Books',
                           json={
                               "ID": 255,
                               "Title": "hello",
                               "Description": "a",
                               "PageCount": 78,
                               "Excerpt": "b",
                               "PublishDate": "2020-02-22T09:36:32.5138932+00:00"
                           }).json()
print(response_1)

# post user
response_2 = requests.post('https://fakerestapi.azurewebsites.net/api/Users',
                           json={
                               "ID": 774,
                               "UserName": "Stas",
                               "Password": "5411"
                           }).json()
print(response_2)

# put book id : 10
response_3 = requests.put('https://fakerestapi.azurewebsites.net/api/Books/10',
                          json={
                              "ID": 10,
                              "Title": "q",
                              "Description": "w",
                              "PageCount": 41,
                              "Excerpt": "e",
                              "PublishDate": "2020-02-22T10:26:30.8951059+00:00"
                          }).json()
print(response_3)

# del user 4
response_4 = requests.put('https://fakerestapi.azurewebsites.net/api/Users/4').json()
print(response_4)
