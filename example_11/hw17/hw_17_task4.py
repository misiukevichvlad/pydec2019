import yaml
import json

with open('order.yaml') as f:
    templates = yaml.safe_load(f)


def order_number(data):
    for i in data:
        if i == 'invoice':
            print(data[i])


def address(data):
    for i in data:
        if i == 'bill-to':
            for n in data[i]:
                if n == 'address':
                    print(data[i][n])


def products_and_total(data):
    for i in data:
        if i == 'product':
            print(data[i])
        if i == 'total':
            print(data[i])


# convert to json
with open('order.json', 'w') as f:
    json.dump(templates, f, default=str)

# create hw_17.yaml
data1 = dict(
    A='a',
    B=dict(
        C='c',
        D='d',
        E='e',
    )
)

with open('hw_17.yaml', 'w') as outfile:
    yaml.dump(data1, outfile, default_flow_style=False)

order_number(templates)
address(templates)
products_and_total(templates)
